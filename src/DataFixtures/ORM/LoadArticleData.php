<?php

namespace App\DataFixtures\ORM;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadArticleData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $article = new Article();
        $article->setName('FYGO');
        $article->setDescription('TUTUROON');
        $article->setCreatedAt(new \DateTime());
        $manager->persist($article);

        $article = new Article();
        $article->setName('NET Beans');
        $article->setDescription('NFSDCSD');
        $article->setCreatedAt(new \DateTime());
        $manager->persist($article);

        $manager->flush();
    }
}
