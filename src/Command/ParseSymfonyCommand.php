<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use App\Entity\NamespaceSymfony;
use App\Entity\InterfaceSymfony;
use App\Entity\ClassesSymfony;

class ParseSymfonyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var
     */
    private $url = 'https://api.symfony.com/4.1/index.html';

    /**
     * ParseSymfonyCommand constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:parsesymfony')
            ->setDescription('Parsing site api.symfony.com')
            ->setHelp('This command parses the site ...')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('test', 't', InputOption::VALUE_OPTIONAL),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $test = $input->getOption('test');

        $namespace = new NamespaceSymfony();
        $namespace->setUrl('https://api.symfony.com/4.1/Symfony.html');
        $namespace->setName('Symfony');
        $this->em->persist($namespace);
        $this->getContent('https://api.symfony.com/4.1/Symfony.html', $namespace, $test);
    }

    /**
     * @param $url= 'https://api.symfony.com/4.1/index.html'
     */
    public function getContent($url, NamespaceSymfony $parent, $test = false)
    {
        $html = file_get_contents($url);
        $crawler = new Crawler($html);

        $filteredInterface = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > em > a > abbr');
        foreach ($filteredInterface as $value1) {
            // save Interface
            $urlInterface = 'https://api.symfony.com/4.1/'.str_replace('\\', '/', $value1->getAttribute('title').'.html');
            $setInterface = new InterfaceSymfony();
            $setInterface->setName($value1->textContent);
            $setInterface->setUrl($urlInterface);
            $setInterface->setNamespaceSymfony($parent);
            $this->em->persist($setInterface);
            $this->em->flush();
        }

        $filteredClasses = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > a');
        foreach ($filteredClasses as $value2) {
            // save classes
            $urlClasses = 'https://api.symfony.com/4.1/'.str_replace('../', '', $value2->getAttribute('href'));
            $setClasses = new ClassesSymfony();
            $setClasses->setName($value2->textContent);
            $setClasses->setUrl($urlClasses);
            $setClasses->setNamespaceSymfony($parent);
            $this->em->persist($setClasses);
        }

        $filtered = $crawler->filter('div#page-content > div.namespace-list > a');
        foreach ($filtered as $value) {
            $urlNamespace = 'https://api.symfony.com/4.1/'.str_replace('../', '', $value->getAttribute('href'));
            $setNamespace = new NamespaceSymfony();
            $setNamespace->setName($value->textContent);
            $setNamespace->setUrl($urlNamespace);
            $setNamespace->setParent($parent);
            $this->em->persist($setNamespace);
            $this->em->flush();
            if ($test) {
                exit;
            }
            $this->getContent($urlNamespace, $setNamespace);
        }
        $this->em->flush();
    }
}
