<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassesSymfony.
 *
 * @ORM\Entity
 */
class ClassesSymfony
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NamespaceSymfony", inversedBy="classesSymfony")
     */
    private $namespaceSymfony;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getNamespaceSymfony()
    {
        return $this->namespaceSymfony;
    }

    /**
     * @param mixed $namespaceSymfony
     */
    public function setNamespaceSymfony($namespaceSymfony): void
    {
        $this->namespaceSymfony = $namespaceSymfony;
    }
}
