<?php

namespace tests\App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testShowArticle()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $ButtonCrawlerNote = $crawler->selectButton('submit');
        $form = $ButtonCrawlerNote->form([
            'Username' => 'VanDan',
            'Password' => '44760',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/article/show/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateArticle()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $ButtonCrawlerNote = $crawler->selectButton('submit');
        $form = $ButtonCrawlerNote->form([
            'Username' => 'VanDan',
            'Password' => '44760',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/article/create');

        $buttonCrawlerNode = $crawler->selectButton('article[submit]');

        $form = $buttonCrawlerNode->form([
            'article[_token]' => 'Apollo442',
            'article[description]' => 'This is fiasko',
            'article[created_at][date][month]' => '05',
            'article[created_at][date][day]' => '02',
            'article[created_at][date][year]' => '2019',
            'article[created_at][time][hour]' => '12',
            'article[created_at][time][minute]' => '59',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testUpdateArticle()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $ButtonCrawlerNote = $crawler->selectButton('submit');
        $form = $ButtonCrawlerNote->form([
            'Username' => 'VanDan',
            'Password' => '44760',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/article/update/61');

        $buttonCrawlerNode = $crawler->selectButton('article[submit]');

        $form = $buttonCrawlerNode->form([
            'article[name]' => 'VINTED',
            'article[description]' => 'FRASH',
            'article[created_at][date][month]' => '12',
            'article[created_at][date][day]' => '6',
            'article[created_at][date][year]' => '2020',
            'article[created_at][time][hour]' => '13',
            'article[created_at][time][minute]' => '40',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testDeleteArticle()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $ButtonCrawlerNote = $crawler->selectButton('submit');
        $form = $ButtonCrawlerNote->form([
            'Username' => 'VanDan',
            'Password' => '44760',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
