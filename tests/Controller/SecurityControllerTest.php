<?php

namespace tests\App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $ButtonCrawlerNote = $crawler->selectButton('submit');
        $form = $ButtonCrawlerNote->form([
            'Username' => 'VanDan',
            'Password' => '44760',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
