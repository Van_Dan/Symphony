<?php

namespace tests\App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegisterControllerTest extends WebTestCase
{
    public function testRegister()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/create_user');
        $ButtonCrawlerNote = $crawler->selectButton('register[Submit]');
        $form = $ButtonCrawlerNote->form([
            'register[username]' => 'Donald',
            'register[password][first]' => '5545',
            'register[password][second]' => '5545',
        ]);
        $client->submit($form);

        $crawler = $client->request('GET', '/article/login');
        $ButtonCrawlerNote = $crawler->selectButton('submit');
        $form = $ButtonCrawlerNote->form([
            'Username' => 'Donald',
            'Password' => '5545',
        ]);
        $client->submit($form);

        $client->request('GET', '/article/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
