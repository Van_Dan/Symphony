<?php

namespace tests\App\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $this->markTestSkipped();
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('app:parsesymfony --test=1');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $output = $commandTester->getDisplay();
        $this->assertEquals('', $output);
    }
}
